version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Springtime",
    description = "A contested valley flourishing during the spring season.\r\n\r\nMap is made by (Jip) Willem Wijnia. \r\nMap is made for RD.\r\n\r\nTextures are from www.textures.com\r\n\r\nCopyright 2021 by (Jip) Willem Wijnia. CC-BY-SA-NC 4.0.",
    preview = '',
    map_version = 5,
    type = 'skirmish',
    starts = true,
    size = {1024, 1024},
    reclaim = {29230.3, 126086.2},
    map = '/maps/Springtime/Springtime.scmap',
    save = '/maps/Springtime/Springtime_save.lua',
    script = '/maps/Springtime/Springtime_script.lua',
    norushradius = 40,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2', 'ARMY_3', 'ARMY_4', 'ARMY_5', 'ARMY_6', 'ARMY_7', 'ARMY_8'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
