Decals = {
    --
    ['9005'] = {
        type = 'Albedo',
        name0 = '/maps/springtime/env/decals/light.dds',
        name1 = '',
        scale = { 1024, 1024, 1024 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
    --
}
