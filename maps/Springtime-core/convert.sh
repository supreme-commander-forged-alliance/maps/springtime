#!/bin/bash

output=$1
if [ -z "$output" ]
  then
    output="../maps/Springtime/env/decals"
fi

for entry in "output/"*;
do
    # determine old file location
    fileOld="${entry%.*}"
    ddsOld="$fileOld.dds"

    # determine new file location
    fileNew=`basename $fileOld`
    ddsNew="$output/$fileNew.dds"

    # only change if file we want to convert is newer
    if [[ "$entry" -nt "$ddsNew" ]] ; then
        # convert the file
        echo "Converting: $entry"
        magick "$entry" -define dds:compression=dxt5 "$ddsOld"

        # move it to the new location
        mv "$ddsOld" "$ddsNew"
    else
        echo "Skipping: $entry (not updated)"
    fi
done

