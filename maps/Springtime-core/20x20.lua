Decals = {
    --
    ['9001'] = {
        type = 'Normals',
        name0 = '/maps/springtime.v0001/env/decals/normals.dds',
        name1 = '',
        scale = { 1024, 1024, 1024 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
    --
    ['9002'] = {
        type = 'Albedo',
        name0 = '/maps/springtime.v0001/env/decals/light.dds',
        name1 = '',
        scale = { 1024, 1024, 1024 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
    --
    ['9003'] = {
        type = 'Albedo',
        name0 = '/maps/springtime.v0001/env/decals/ambient.dds',
        name1 = '',
        scale = { 1024, 1024, 1024 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
    --
}
