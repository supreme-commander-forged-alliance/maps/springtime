## Springtime

_A map for Supreme Commander: Forged Alliance_

![](images/marked_preview.jpg)

## Statistics of the map

The map is a remake of 'Valley Passage' - a map widely played in the [LOUD](https://www.moddb.com/mods/loud-ai-supreme-commander-forged-alliance) community. Therefore the design may be off for the typical balance of Supreme Commander - the balance in LOUD is quite different.

![](images/preview-1.png)

There is reclaim on this map. This includes:
 - A lot of trees for energy.
 - A lot of rocks near mountains for mass.

## Technical aim of the map

For the community new techniques are applied to create a stunning visual result. We applied similar techniques that the spring engine applies - using map-wide albedo, normal and specular textures. 

![](images/preview-2.png)

These textures are generated via procedural software. The core version of the map represents a raw version of the map - we (ab)use the Ozone editor to create a heightmap and various layers that can be used as a mask. These are all input for World Machine that turns the raw terrain into a more nature-isch version of itself.

![](images/preview-3.png)

The world machine file is included in the git. All the input and output files are excluded - you can generate these from the map itself (input) and through World Machine (output). The output can then be used on the map without further changes to get the exact same map. The procedure is as follows:
 - Export the heightmap and (used) stratum layers from the core map.
 - Import them accordingly in World Machine. In turn, use World Machine to generate all kinds of output. These need to be done one-by-one as the output of one set of nodes may be the input of another set of nodes.
 - Transform the format from `.png` or `.tif` into `.dds` with software such as Gimp or Adobe Photoshop.

![](images/preview-4.png)

The aim was to show that a map can be both visually interesting and competitively easy to read. As an example, the albedo is often used to make slight changes in the terrain easier to read.

![](images/preview-5.png)